var
	CurrentObj	= '',
	CurrentPos	= 0,
	CurrentLayer	= '',
	ActiveChapterId = '';

function hideArrows () {
	document.getElementById('left-arrow').style.display = 'none';
	document.getElementById('right-arrow').style.display = 'none';
}
function showArrows () {
	document.getElementById('left-arrow').style.display = 'block';
	document.getElementById('right-arrow').style.display = 'block';
}

function showAnswer ( ChapterName, QuestionNumber ) {

	var NewLayer = createLayer ( 'content', fillTemplate ( 'template', QuestionsAndAnswers[ ChapterName ][ QuestionNumber ] ) );

	if ( CurrentLayer ) document.body.removeChild ( CurrentLayer );
	CurrentLayer = NewLayer;
	CurrentPos = QuestionNumber;
	CurrentObj = QuestionsAndAnswers[ ChapterName ];

	surr.key.listenON ();
	showArrows();

	Chapter = document.getElementById( ActiveChapterId );
	Chapter.className = 'menu-item  active';
	Chapter.getElementsByTagName('span')[0].className = 'underlined';
}

function showChapterContent ( ChapterId, ChapterName ) {

	Chapter = document.getElementById( ChapterId );
	if ( /choosen/.test( Chapter.className ) ) return;

	surr.key.listenOFF ();
	hideArrows();

	setActiveChapter ( Chapter );
	CurrentMenu = '';
	for ( var pos in QuestionsAndAnswers[ ChapterName ] ) {
		CurrentMenu += fillTemplate (
			'undermenu-item',
			{ 'CHAPTER':ChapterName, 'ITEM_NUMBER':pos, 'ITEM':QuestionsAndAnswers[ ChapterName ][pos]['QUESTION'] }
		)
	}
	CurrentMenu = fillTemplate ( 'undermenu', { 'UNDERMENU':CurrentMenu } )
	if ( CurrentLayer ) document.body.removeChild ( CurrentLayer );
	CurrentLayer = createLayer ( 'content', CurrentMenu );
}

function setActiveChapter ( Chapter ) {
	
	Chapter.className	= 'menu-item  active choosen';
	Chapter.getElementsByTagName('span')[0].className = '';
	if ( ActiveChapterId && ( ActiveChapterId != Chapter.id ) ) {
		ActiveChapter = document.getElementById( ActiveChapterId );
		ActiveChapter.className	= 'menu-item ';
		ActiveChapter.getElementsByTagName('span')[0].className = 'underlined';
	}
	ActiveChapterId = Chapter.id;
}

function fillTemplate ( Id, Obj ) {
	myTemplate = document.getElementById( Id ).innerHTML;
	return myTemplate.replace( /%([^%]+)%/mg, function( str, elem ) { return Obj [ elem ] ? Obj [ elem ] : '' } )
}

function createLayer ( ClassName, content, where ) {
	var CoordX =	where == 'left'		? '-100%'	:
			where == 'right'	? '100%'	: '0%';
	NewLayer = document.createElement('div');
	NewLayer.className	= ClassName;
	NewLayer.innerHTML	= content;
	NewLayer.left		= CoordX;
	document.body.appendChild ( NewLayer );
	return NewLayer
}

function scrollLayer ( where ) {
	var k = where == 'left' ? -1 : 1;
	if ( ! CurrentObj [ CurrentPos + k ] ) return;
	CurrentPos += k;
	NewLayer = createLayer ( 'content', fillTemplate ( 'template', CurrentObj [ CurrentPos ] ), where );
	if ( k == -1 ) moveRight ( NewLayer, CurrentLayer );
	else moveLeft ( NewLayer, CurrentLayer );
}

function moveLeft ( New_obj, Old_obj, Old_obj_x ) {
	Old_obj_x = Old_obj_x || 0;
	if ( Old_obj_x > -100 ) Old_obj_x -= 5; else { document.body.removeChild( CurrentLayer ); CurrentLayer = New_obj; return }
	Old_obj.style.left = Old_obj_x + '%';
	New_obj.style.left = Old_obj_x + 100 + '%';
	setTimeout ( function(){moveLeft (New_obj, Old_obj, Old_obj_x)}, 10 );
}

function moveRight ( New_obj, Old_obj, Old_obj_x ) {
	Old_obj_x = Old_obj_x || 0;
	if ( Old_obj_x < 100 ) Old_obj_x += 5; else { document.body.removeChild( CurrentLayer ); CurrentLayer = New_obj; return }
	Old_obj.style.left = Old_obj_x + '%';
	New_obj.style.left = Old_obj_x - 100 + '%';
	setTimeout ( function(){moveRight (New_obj, Old_obj, Old_obj_x)}, 10 );
}

function createMenu ( Obj ) {
	var
		IdBase		= 'chapter_',
		IdNumber	= 0,
		Menu		= '';
	
	for ( Chapter in Obj ) {
		IdNumber++;
		var Id = IdBase + IdNumber;
		Menu += fillTemplate ( 'mainmenu-item', { 'TEXT':Chapter, 'ID':Id } );
	}
	Menu = fillTemplate ( 'mainmenu', { 'MAINMENU':Menu } );
	createLayer ( 'menu-wrapper', Menu );

	// some govnokode for fragrance
	var ItemWidth = 88 / IdNumber;
	for ( ; IdNumber > 0; IdNumber-- ) document.getElementById( IdBase + IdNumber ).style.width = ItemWidth + '%';
}

createMenu ( QuestionsAndAnswers );

surr.key.setHandler ( function(){ scrollLayer ('left') } , 37);
surr.key.setHandler ( function(){ scrollLayer ('left') } , 17, 37);
surr.key.setHandler ( function(){ scrollLayer ('right') } , 39);
surr.key.setHandler ( function(){ scrollLayer ('right') } , 17, 39);