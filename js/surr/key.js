// DESCRIPTION:
// gives simple interface to set handler functions for multiplay pushed buttons ( Ctrl+arrows, Ctrl+Alt+Shift+…, 'q'+'w', etc )
//
// USAGE:
// set handlers:
//		surr.key.setHandler( <handler function>, keycode [ , keycode... ] );
// as many functions and keycodes, as you need
// and start listening:
//	surr.key.listenON ();
// stop if you need:
//	surr.key.listenOFF ();
//
// read more in next comment block
//
// EXAMPLE:
// 	surr.key.setHandler ( function(){console.log('Ctrl+left')} , 17,37);
// 	surr.key.setHandler ( function(){console.log('Ctrl+right')} , 17,39);
// 	surr.key.setHandler ( function(){console.log('Ctrl+Shift+left')} , 16,17,37);
// 	surr.key.setHandler ( function(){console.log('Ctrl+Shift+right')} , 16,17,39);
// 	surr.key.setHandler ( function(){ surr.key.listenOFF() } , 27); 		// stop listening when Esc is pressed
// 	surr.key.listenON ();								// start

var surr;
if ( !surr ) surr = {}
else if ( typeof surr != 'object' ) throw new Error ( "[surr.key]: the name 'surr' exists, but is not 'object'" );
surr.key;
if ( !surr.key ) surr.key = {}
else if ( typeof surr.key != 'object' ) throw new Error ( "[surr.key]: the name 'surr.TheLostWorld' exists, but is not 'object'" );

surr.key = {

	Handlers	: {},
	ClearAtOnce	: false,	// set it 'true' if there is 'blocking function' ( like alert() ) in Handlers
					// in that case user could not, for example, push several times arrow buttons holding 'Ctrl'
	StillPushed	: false,	// flag ( true if keys are still pushed :) use it with 'PushedNow' to determinate —
	PushedNow	: {},		// what exactly pushed now ( if you need )

	pushed : function ( code ) {
		if ( surr.key.StillPushed ) return;
		surr.key.PushedNow[ code ] = 1;
		surr.key.findHandler( code );
	},

	findHandler : function ( code ) {
		if ( ! surr.key.Handlers[code] ) return;
		var obj = surr.key.Handlers[code];
		for ( var PushedCode in surr.key.PushedNow ) {
			if ( PushedCode == code ) continue;
			if ( obj.o && obj.o[PushedCode] ) obj = obj.o[PushedCode];
			else {
				delete surr.key.PushedNow[PushedCode];						// TEST
				if ( surr.key.PushedNow.length == 0 ) surr.key.StillPushed = false;		// TEST
				return
			}
		}
		if ( obj.f ) {
			surr.key.StillPushed = true;
// console.log ( 'pushed' )
			obj.f();
			if ( surr.key.ClearAtOnce ) {
				surr.key.PushedNow = {};
				surr.key.StillPushed = false;
// console.log ( 'released' )
			}
		}
	},

	released : function ( code ) {
		delete surr.key.PushedNow[ code ];
		surr.key.StillPushed = false;
// console.log ( 'released' );
	},
	listenON : function () {
		document.onkeydown = function ( event ) { event = event || window.event; surr.key.pushed( event.keyCode ) }
		document.onkeyup = function ( event ) { event = event || window.event; surr.key.released( event.keyCode ) }
	},
	listenOFF : function () {
		document.onkeydown = document.onkeyup = null;
	},
	CurrentHandler : '',

	setHandler : function ( handler ) {
		var codes = [].slice.call(arguments, 1);
		surr.key.CurrentHandler = handler;
		surr.key.recurse ( surr.key.Handlers, codes );
	},
	recurse : function ( obj, codes ) {
		if ( codes.length > 1 ) {
			for ( var i in codes ) {
				if ( !obj[ codes[i] ] ) obj[ codes[i] ] = {}
				if ( !obj[ codes[i] ].o) obj[ codes[i] ].o = {};
				// take all rest
				var rest = [];
				for ( var k in codes ) { if ( codes[k] !== codes[i] ) rest.push(codes[k]) }
				// and send it farther
				surr.key.recurse ( obj[ codes[i] ].o, rest );
			}
		} else {
			if ( !obj[ codes[0] ] ) obj[ codes[0] ] = {}
			obj[ codes[0] ].f = surr.key.CurrentHandler;
		}
	}
}